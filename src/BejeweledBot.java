import java.awt.*;
import java.util.TreeMap;

public class BejeweledBot {

    public static void main(String[] args){
        boolean exit = false;
        Board board = new Board();
        Solver solver = new Solver();

        while (!exit) {
            try{
 //         a.print();
                System.out.println();
                System.out.println();
            int[] data = board.getSData();
            TreeMap scoreBoard = solver.solve(board.getGameBoard(data));
            scoreBoard.forEach((key, value) -> System.out.println(key));
                board.moveTile(scoreBoard);
            scoreBoard.clear();
            Thread.sleep(100);
            testPosition(800);

        }
        catch (IllegalArgumentException ie)
        {exit = true;
        System.out.print("WARNING!!!");}
        catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
    private static void testPosition(int x) throws IllegalArgumentException{
        Point p = MouseInfo.getPointerInfo().getLocation();
        if(p.x < x) throw new IllegalArgumentException();
    }
}
