import java.util.Arrays;
import java.util.TreeMap;

public class Solver {
    private boolean annihilated = true;
    TreeMap<Integer,int[][]> scoreBoard = new TreeMap();

    public TreeMap solve(int[][] gameBoard){
        for (int y = 7; y >= 0; y--) {
            for (int x = 7; x >= 0; x--) {
//      X
//      X
//     X X
//      X
                if (x-1 >= 0 && y - 3 >= 0 && x+1 <= 7) {
                    if (gameBoard[x][y] == gameBoard[x-1][y-1] && gameBoard[x][y] == gameBoard[x+1][y-1] && gameBoard[x][y] == gameBoard[x][y-2] && gameBoard[x][y] == gameBoard[x][y-3]) {
                        scoreBoard.put(scorePoints(gameBoard, getMove(x,y,x,y-1)), getMove(x,y,x,y-1));
                    }
//      X
//     X X
//      X
//      X
                    if (gameBoard[x][y] == gameBoard[x][y-1] && gameBoard[x][y] == gameBoard[x-1][y-2] && gameBoard[x][y] == gameBoard[x+1][y-2] && gameBoard[x][y] == gameBoard[x][y-3]) {
                        scoreBoard.put(scorePoints(gameBoard, getMove(x,y-3,x,y-2)), getMove(x,y-3,x,y-2));
                    }
                }
//      X
//     X XX
//      X
//
                if (x+2 <= 7 && x-1 >=0 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x-1][y-1] && gameBoard[x][y] == gameBoard[x+1][y - 1] && gameBoard[x][y] == gameBoard[x][y-2] && gameBoard[x][y] == gameBoard[x+2][y-1]) {
                        scoreBoard.put(scorePoints(gameBoard, getMove(x-1,y-1,x,y-1)), getMove(x-1,y-1,x,y-1));
                    }
                }
//      X
//    XX X
//      X
//
                if (x+1 <= 7 && x-2 >=0 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x-1][y-1] && gameBoard[x][y] == gameBoard[x+1][y - 1] && gameBoard[x][y] == gameBoard[x][y-2] && gameBoard[x][y] == gameBoard[x-2][y-1]) {
                        //System.out.println("38");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x+1,y-1,x,y-1)), getMove(x+1,y-1,x,y-1));
                    }
                }
//
//     X XX
//      X
//      X
                if (x+2 <= 7 && x-1 >=0 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x][y-1] && gameBoard[x][y] == gameBoard[x+1][y - 2] && gameBoard[x][y] == gameBoard[x-1][y-2] && gameBoard[x][y] == gameBoard[x+2][y-2]) {
                        //System.out.println("37");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x-1,y-2,x,y-2)), getMove(x-1,y-2,x,y-2));
                    }
                }
//
//    XX X
//      X
//      X
                if (x +1 <= 7 && x-2 >=0 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x][y-1] && gameBoard[x][y] == gameBoard[x+1][y - 2] && gameBoard[x][y] == gameBoard[x-1][y-2] && gameBoard[x][y] == gameBoard[x-2][y-2]) {
                        //System.out.println("36");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x+1,y-2,x,y-2)), getMove(x+1,y-2,x,y-2));
                    }
                }
//      X
//      X
//     X XX
//
                if (x-3 >= 0 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x-1][y] && gameBoard[x][y] == gameBoard[x-3][y] && gameBoard[x][y] == gameBoard[x-2 ][y-1] && gameBoard[x][y] == gameBoard[x-2][y-2]) {
                        //System.out.println("35");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x-3,y,x-2,y)), getMove(x-3,y,x-2,y));
                    }
//      X
//      X
//    XX X
//
                    if (gameBoard[x][y] == gameBoard[x-2][y] && gameBoard[x][y] == gameBoard[x-3][y] && gameBoard[x][y] == gameBoard[x-1][y-1] && gameBoard[x][y] == gameBoard[x-1][y-2]) {
                        //System.out.println("34");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x,y,x-1,y)), getMove(x,y,x-1,y));
                    }
                }
//      X
//      X
//    XX
//      X
                if (x-2 >= 0 && y - 3 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x-1][y-1] && gameBoard[x][y] == gameBoard[x-2][y - 1] && gameBoard[x][y] == gameBoard[x ][y-2] && gameBoard[x][y] == gameBoard[x ][y-3]) {
                        //System.out.println("33");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x,y,x,y-1)), getMove(x,y,x,y-1));
                    }
//      X
//    XX
//      X
//      X
                    if (gameBoard[x][y] == gameBoard[x][y-1] && gameBoard[x][y] == gameBoard[x-1][y - 2] && gameBoard[x][y] == gameBoard[x-2 ][y-2] && gameBoard[x][y] == gameBoard[x ][y-3]) {
                        //System.out.println("32");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x,y-3,x,y-2)), getMove(x,y-3,x,y-2));
                    }
                }
//      X
//      X
//       XX
//      X
                if (x +2 <= 7 && y - 3 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x+1][y-1] && gameBoard[x][y] == gameBoard[x+2][y - 1] && gameBoard[x][y] == gameBoard[x ][y-2] && gameBoard[x][y] == gameBoard[x ][y-3]) {
                        //System.out.println("31");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x,y,x,y-1)), getMove(x,y,x,y-1));
                    }
//      X
//       XX
//      X
//      X
                    if (gameBoard[x][y] == gameBoard[x][y-1] && gameBoard[x][y] == gameBoard[x+1][y - 2] && gameBoard[x][y] == gameBoard[x+2 ][y-2] && gameBoard[x][y] == gameBoard[x][y-3]) {
                        //System.out.println("30");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x,y-3,x,y-2)), getMove(x,y-3,x,y-2));
                    }
                }
//      X
//      X
//     X
//      X
//      X
                if (y - 4 >= 0 && x - 1 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x][y-1] && gameBoard[x][y] == gameBoard[x -1][y - 2] && gameBoard[x][y] == gameBoard[x ][y-3] && gameBoard[x][y] == gameBoard[x ][y-4]) {
                        //System.out.println("29");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x-1,y-2,x,y-2)), getMove(x-1,y-2,x,y-2));
                    }
                }
//      X
//      X
//       X
//      X
//      X
                if (y - 4 >= 0 && x + 1 <= 7) {
                    if (gameBoard[x][y] == gameBoard[x][y-1] && gameBoard[x][y] == gameBoard[x +1][y - 2] && gameBoard[x][y] == gameBoard[x ][y-3] && gameBoard[x][y] == gameBoard[x ][y-4]) {
                        //System.out.println("28");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x+1,y-2,x,y-2)), getMove(x+1,y-2,x,y-2));
                    }
                }
//    XX XX
//      X
                if (x - 4 >= 0 && y + 1 <= 7) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y + 1] && gameBoard[x][y] == gameBoard[x - 3][y] && gameBoard[x][y] == gameBoard[x - 4][y]) {
                        //System.out.println("27");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x-2,y+1,x-2,y)), getMove(x-2,y+1,x-2,y));
                    }
                }
//       X
//     XX XX
                if (x - 4 >= 0 && y - 1 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y - 1] && gameBoard[x][y] == gameBoard[x - 3][y] && gameBoard[x][y] == gameBoard[x - 4][y]) {
                        //System.out.println("26");
                        scoreBoard.put(scorePoints(gameBoard, getMove(x-2,y-1,x-2,y)), getMove(x-2,y-1,x-2,y));
                    }
                }

//       X
//     XX X
                if (x - 3 >= 0 && y - 1 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x - 2][y] && gameBoard[x][y] == gameBoard[x - 3][y]) {
                        //System.out.println("25");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-1,y-1,x-1,y)),getMove(x-1,y-1,x-1,y));
                    }
//      X
//     X XX
                    if (gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y - 1] && gameBoard[x][y] == gameBoard[x - 3][y]) {
                        //System.out.println("24");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-2,y-1,x-2,y)),getMove(x-2,y-1,x-2,y));

                    }
                }
//      X
//       X
//      X
//      X
                if (x + 1 <= 7 && y - 3 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x + 1][y - 2] && gameBoard[x][y] == gameBoard[x][y - 3]) {
                        //System.out.println("23");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x+1,y-2,x,y-2)),getMove(x+1,y-2,x,y-2));

                    }

//     X
//     X
//      X
//     X
                    if (gameBoard[x][y] == gameBoard[x + 1][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x][y - 3]) {
                        //System.out.println("22");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x+1,y-1,x,y-1)),getMove(x+1,y-1,x,y-1));

                    }
                }

//      X
//     X
//      X
//      X
                if (x - 1 >= 0 && y - 3 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x - 1][y - 2] && gameBoard[x][y] == gameBoard[x][y - 3]) {
                        //System.out.println("21");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-1,y-2,x,y-2)),getMove(x-1,y-2,x,y-2));

                    }

//     X
//     X
//    X
//     X
                    if (gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x][y - 3]) {
                        //System.out.println("20");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-1,y-1,x,y-1)),getMove(x-1,y-1,x,y-1));

                    }
                }

//     XX X
//       X
                if (x - 2 >= 0 && y - 1 >= 0 && x + 1 <= 7) {
                    if (gameBoard[x][y] == gameBoard[x + 1][y - 1] && gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x - 2][y - 1]) {
                        //System.out.println("19");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x,y-1)),getMove(x,y,x,y-1));

                    }
                }

//     X XX
//      X
                if (x - 1 >= 0 && y - 1 >= 0 && x + 2 <= 7) {
                    if (gameBoard[x][y] == gameBoard[x + 1][y - 1] && gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x + 2][y - 1]) {
                        //System.out.println("18");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x,y-1)),getMove(x,y,x,y-1));

                    }
                }


//         X
//        X X
                if (x - 2 >= 0 && y - 1 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x - 2][y]) {
                        //System.out.println("17");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-1,y-1,x-1,y)),getMove(x-1,y-1,x-1,y));
                    }
                }
//        X
//         X
//        X
                if (x + 1 <= 7 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x + 1][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2]) {
                        //System.out.println("16");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x+1,y-1,x,y-1)),getMove(x+1,y-1,x,y-1));


                    }
                }
//         X X
//          X
                if (x - 2 >= 0 && y-1>=0 && y + 1 <= 7) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y + 1] && gameBoard[x][y] == gameBoard[x - 2][y]) {
                        //System.out.println("15");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-1,y+1,x-1,y)),getMove(x-1,y+1,x-1,y));
                    }
                }
//        X
//       X
//        X
                if (x - 1 >= 0 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2]) {
                        //System.out.println("14");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-1,y-1,x,y-1)),getMove(x-1,y-1,x,y-1));
                    }
                }
//
//       XX X
//
                if (x - 3 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x - 2][y] && gameBoard[x][y] == gameBoard[x - 3][y]) {
                        //System.out.println("13");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x-1,y)),getMove(x,y,x-1,y));

                    }

//       X XX
//
                    if (gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 3][y]) {
                        //System.out.println("12");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-3,y,x-2,y)),getMove(x-3,y,x-2,y));

                    }
                }
//       X
//
//       X
//       X
                if (y - 3 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 3]) {
                        //System.out.println("11");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y-3,x,y-2)),getMove(x,y-3,x,y-2));

                    }

//       X
//       X
//
//       X
                    if (gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x][y - 3]) {
                        //System.out.println("10");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x,y-1)),getMove(x,y,x,y-1));

                    }
                }
                //
//         X
//       XX
                if (x - 2 >= 0 && y + 1 <= 7) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y + 1] && gameBoard[x][y] == gameBoard[x - 2][y + 1]) {
                        //System.out.println("9");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x,y+1)),getMove(x,y,x,y+1));

                    }

//       XX
//      X
                    if (gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y + 1]) {
                        //System.out.println("8");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-2,y+1,x-2,y)),getMove(x-2,y+1,x-2,y));

                    }
                }

//      X
//       XX
                if (x - 2 >= 0 && y - 1 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y - 1]) {
                        //System.out.println("7");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-2,y-1,x-2,y)),getMove(x-2,y-1,x-2,y));

                    }

//       XX
//         X
                    if (gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x - 2][y - 1]) {
                        //System.out.println("6");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x,y-1)),getMove(x,y,x,y-1));

                    }
                }

//      X
//      X
//       X
                if (x - 1 >= 0 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x - 1][y-1] && gameBoard[x][y] == gameBoard[x - 1][y - 2]) {
                        //System.out.println("5");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x-1,y)),getMove(x,y,x-1,y));

                    }
//        X
//         X
//         X
                    if (gameBoard[x][y] == gameBoard[x ][y - 1] && gameBoard[x][y] == gameBoard[x - 1][y - 2]) {
                        //System.out.println("4");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x-1,y-2,x,y-2)),getMove(x-1,y-2,x,y-2));

                    }
                }
//      X
//      X
//     X
                if (x + 1 <= 7 && y - 2 >= 0) {
                    if (gameBoard[x][y] == gameBoard[x + 1][y-1] && gameBoard[x][y] == gameBoard[x + 1][y - 2]) {
                        //System.out.println("3");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x,y,x+1,y)),getMove(x,y,x+1,y));

                    }
//          X
//         X
//         X
                    if (gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x + 1][y - 2]) {
                        //System.out.println("2");
                        scoreBoard.put(scorePoints(gameBoard,getMove(x+1,y-2,x,y-2)),getMove(x+1,y-2,x,y-2));

                    }
                }
            }
        }
        return scoreBoard;
    }

    private int scorePoints(int[][] gameBoard, int[][] toSolve){
        int[][] newGameBoard = clone2DArray(gameBoard);
        moveInMind(newGameBoard, toSolve);
        while(annihilated){
        annihilate(newGameBoard);
        dropDown(newGameBoard);}
        int score = 0;
        annihilated = true;
        for(int y = 7; y>=0 ; y--){
            for(int x = 7; x>=0 ; x--) {
                if(newGameBoard[x][y]==9){score++;}}
            }
        
        return score;
        
    }

    private int[][] clone2DArray(int[][] gameBoard) {
        int[][] newGameBoard = gameBoard.clone();
        for(int i = 0; i<8; i++){
            newGameBoard[i]= gameBoard[i].clone();
        }
        return newGameBoard;
    }

    private int[][] dropDown(int[][] gameBoard){
        boolean exit = false;
        while(!exit){
        int gameBoardOld = Arrays.deepHashCode(gameBoard);
        for(int i =0;i<4;i++){
            for(int y = 7; y>0 ; y--){
                for(int x = 7; x>=0 ; x--) {
                    if (gameBoard[x][y] == 9&& gameBoard[x][y-1]!=9) {
                    gameBoard[x][y] = gameBoard[x][y - 1];
                    gameBoard[x][y - 1] = 9;
                    }   
                }
            }
        }
        int gameBoardNew = Arrays.deepHashCode(gameBoard);
        if(gameBoardNew==gameBoardOld) exit=true;
        }

        return gameBoard;
    }
    
    private int[][] moveInMind(int[][] gameBoard, int[][] toSolve){
//        try {
            int temp = gameBoard[toSolve[0][0]][toSolve[0][1]];
            gameBoard[toSolve[0][0]][toSolve[0][1]] = gameBoard[toSolve[1][0]][toSolve[1][1]];
            gameBoard[toSolve[1][0]][toSolve[1][1]] = temp;
 //       }catch (ArrayIndexOutOfBoundsException a){System.out.println("Коммандир, мы вышли за пределы поля... Вот координаты: " + toSolve[0][0] + " " +toSolve[0][1] + " " +toSolve[1][1] + " " +toSolve[1][1]);}
        return gameBoard;
    }
    
    private int[][] annihilate(int[][] gameBoard) {
        int gameBoardOld = Arrays.deepHashCode(gameBoard);
        for (int y = 7; y >= 0; y--) {
            for (int x = 7; x >= 0; x--) {

                if (x + 1 <= 7 && y - 2 >= 0 && x - 1 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x - 1][y - 2] && gameBoard[x][y] == gameBoard[x + 1][y - 2]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        gameBoard[x + 1][y - 2] = 9;
                        gameBoard[x - 1][y - 2] = 9;
                        continue;
                    }
                }
                if (x + 2 <= 7 && y - 2 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x + 1][y - 2] && gameBoard[x][y] == gameBoard[x + 2][y - 2]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        gameBoard[x + 1][y - 2] = 9;
                        gameBoard[x + 2][y - 2] = 9;
                        continue;
                    }
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x + 1][y - 1] && gameBoard[x][y] == gameBoard[x + 2][y - 1]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        gameBoard[x + 1][y - 1] = 9;
                        gameBoard[x + 2][y - 1] = 9;
                        continue;
                    }
                }
                if (x - 2 >= 0 && y - 2 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y] && gameBoard[x][y] == gameBoard[x - 2][y - 1] && gameBoard[x][y] == gameBoard[x - 2][y - 2]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x - 1][y] = 9;
                        gameBoard[x - 2][y] = 9;
                        gameBoard[x - 2][y - 1] = 9;
                        gameBoard[x - 2][y - 2] = 9;
                        continue;
                    }
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y] && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x - 1][y] = 9;
                        gameBoard[x - 2][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        continue;
                    }
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x - 1][y - 2] && gameBoard[x][y] == gameBoard[x - 2][y - 2]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        gameBoard[x - 1][y - 2] = 9;
                        gameBoard[x - 2][y - 2] = 9;
                        continue;
                    }
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y] && gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x - 1][y - 2]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x - 1][y] = 9;
                        gameBoard[x - 2][y] = 9;
                        gameBoard[x - 1][y - 1] = 9;
                        gameBoard[x - 1][y - 2] = 9;
                        continue;
                    }
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x - 1][y - 1] && gameBoard[x][y] == gameBoard[x - 2][y - 1]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        gameBoard[x - 1][y - 1] = 9;
                        gameBoard[x - 2][y - 1] = 9;
                        continue;
                    }
                }
                if (x - 4 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y] && gameBoard[x][y] == gameBoard[x - 3][y] && gameBoard[x][y] == gameBoard[x - 4][y]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x - 1][y] = 9;
                        gameBoard[x - 2][y] = 9;
                        gameBoard[x - 3][y] = 9;
                        gameBoard[x - 4][y] = 9;
                        continue;
                    }
                }
                if (y - 4 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x][y - 3] && gameBoard[x][y] == gameBoard[x][y - 4]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        gameBoard[x][y - 3] = 9;
                        gameBoard[x][y - 4] = 9;
                        continue;
                    }
                }
                if (x - 3 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y] && gameBoard[x][y] == gameBoard[x - 3][y]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x - 1][y] = 9;
                        gameBoard[x - 2][y] = 9;
                        gameBoard[x - 3][y] = 9;
                        continue;
                    }
                }
                if (y - 3 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2] && gameBoard[x][y] == gameBoard[x][y - 3]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        gameBoard[x][y - 3] = 9;
                        continue;
                    }
                }
                if (x - 2 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x - 1][y] && gameBoard[x][y] == gameBoard[x - 2][y]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x - 1][y] = 9;
                        gameBoard[x - 2][y] = 9;
                        continue;
                    }
                }
                if (y - 2 >= 0) {
                    if (gameBoard[x][y] != 9 && gameBoard[x][y] == gameBoard[x][y - 1] && gameBoard[x][y] == gameBoard[x][y - 2]) {
                        gameBoard[x][y] = 9;
                        gameBoard[x][y - 1] = 9;
                        gameBoard[x][y - 2] = 9;
                        continue;
                    }
                }
            }
        }
        int gameBoardNew = Arrays.deepHashCode(gameBoard);
        if(gameBoardNew==gameBoardOld){annihilated=false;}
        return gameBoard;
    }

    private int[][] getMove(int x1, int y1, int x2, int y2){
        int[][] toSolved = new int[2][2];
        toSolved[0] = new int[]{x1, y1};
        toSolved[1] = new int[]{x2, y2};
        return toSolved;
    }
}