import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;
import java.util.Map;
import java.util.TreeMap;

public class Board {
    private int[] idealGemColors = {
            -1118482,  //WHITE,
            -67548,    //YELLOW,
            -1070259,  //ORANGE,
            -15629833, //BLUE,
            -13448362, //GREEN,
            -848141,   //PURPLE,
            -320713    //RED
    };
    private int startBoardX = 1382;
    private int startBoardY = 477;
    private int boardWidth = 512;
    private int boardHeight = 512;
    private int elementWidth = 512/8;
    private int elementHeight = 512/8;
    private Rectangle rect = new Rectangle(startBoardX,startBoardY,boardWidth,boardHeight);
    int[][] gameBoard = new int[8][8];
    static Robot bot;
    static {
        try {
            bot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public int[] getSData(){
        BufferedImage img = bot.createScreenCapture(rect);
        WritableRaster r=img.getRaster();
        DataBuffer db=r.getDataBuffer();
        DataBufferInt dbi=(DataBufferInt)db;
        int[] data=dbi.getData();
        return data;
    }

//    public void print(){
//        gameBoard = getGameBoard(getSData());
//        for (int i=0; i < 8; i++) {
//            System.out.println();
//            for (int j = 0; j < 8; j++){
//                System.out.print(gameBoard[j][i] + " ");
//            }
//        }
//    }

    //Метод возвращает точку элемента по длинне a и высоте b
    public Point getElementPosition(int a, int b){
        int x = startBoardX+(elementWidth/2)+((a)*elementWidth);
        int y = startBoardY+(elementHeight/2)+((b)*elementHeight);
        return new Point(x,y);
    }

    public int[][] getGameBoard(int [] data) {
        for (int i=0; i < 8; i++) {
           for (int j = 0; j < 8; j++){
                    gameBoard[j][i] = getColorInPos(j, i, data);
           }
        }
        return gameBoard;
    }

    public int getColorInPos(int a, int b, int[] data){
        int hashColor = data[((getElementPosition(a,b).x-startBoardX))+((getElementPosition(a,b).y)-startBoardY)*boardWidth];
        return lookupGemColor(hashColor);
    }

    private final double colorDistance(int intColor1, int intColor2) {
        Color color1 = new Color(intColor1);
        Color color2 = new Color(intColor2);

        long rmean = (color1.getRed() + color2.getRed()) / 2;
        long r = color1.getRed() - color2.getRed();
        long g = color1.getGreen() - color2.getGreen();
        long b = color1.getBlue() - color2.getBlue();
        return Math.sqrt(
                (((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8)
        );
    }

    private int lookupGemColor(int gemColor) {
        double minDist = 9999;
        int minIndex = 0;
        double cDist;
        for (int i = 0; i < 7; i++) {
            cDist = colorDistance(gemColor, idealGemColors[i]);
            if (cDist < minDist) {
                minDist = cDist;
                minIndex = i;
            }
        }
        return minIndex;
    }

    public void moveTile(TreeMap scoreBoard) {
        int[][] toSolved;
        try {
            toSolved = (int[][])scoreBoard.lastEntry().getValue();
        } catch (NullPointerException np){System.out.println("There is no moves"); return;}

        Point[] toSolve = {getElementPosition(toSolved[0][0], toSolved[0][1]), getElementPosition(toSolved[1][0], toSolved[1][1])};
        System.out.println(toSolve[0].x + " " + toSolve[0].y + " " + toSolve[1].x + " " + toSolve[1].y);

        Board.bot.mouseMove(toSolve[0].x, toSolve[0].y);
        Board.bot.mousePress(16);
        Board.bot.mouseMove(toSolve[1].x, toSolve[1].y);
        Board.bot.mouseRelease(16);
        Board.bot.mouseMove(1268, 263);
    }
}
